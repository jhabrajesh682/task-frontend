import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TaskComponent } from './task/task.component';
import { FileuploadComponent } from './fileupload/fileupload.component';
import { SidebarComponent } from './sidebar/sidebar.component';
const routes: Routes = [
  // {
  //   path: 'login',
  //   component: LoginComponent
  // },
  // {
  //   path: 'task',
  //   component: TaskComponent
  // },
  // {
  //   path: 'FileUpload',
  //   component: FileuploadComponent
  // },
  // {
  //   path: '',
  //   redirectTo: '/login',
  //   pathMatch: 'full'
  // },

  {
    path: '', component: SidebarComponent, children: [
      { path: 'login', component: LoginComponent},
      { path: 'task', component: TaskComponent },
      { path: 'file', component: FileuploadComponent }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
