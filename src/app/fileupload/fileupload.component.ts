import { Component, OnInit } from '@angular/core';
import { SearchPipe } from '../common/pipe/search.pipe';
import { HttpService } from '../services/http.service';
@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.scss']
})
export class FileuploadComponent implements OnInit {
  closeResult = '';
  taskId: string
  blobnameFile:string
  filterSearch: string;
  SASUrl: string;
  page = 1
  pageSize = 10
  fileId:string
  data: any = []
  rows: any[] = []
  temp: any[] = []
  Files: any;
  updatedFile: string;
  playType: any;
  filechange: any;
  fileName: string;
  fileObj: any = {}
  constructor(private http: HttpService,
    private searchPipe: SearchPipe) { }

  ngOnInit() {
    this.getAllFile();
  }

  search() {
    if (this.filterSearch.length == 0) {
      this.rows = this.temp;
      return;
    } else {
      this.rows = this.searchPipe.transform(this.temp, this.filterSearch);
    }
  }

  addModalData() { }
  editModalData() { }
  getTaskId() { }



  fileChange(event) {

    this.Files = <File>event.target.files[0];

    console.log(this.Files.name)
    console.log(this.Files)
    this.fileName = this.Files.type.slice(0, 5);


    // var allowedExtensions =
    //   ["image"];
    // this.fileExtension = this.fileName

    // if (allowedExtensions.includes(this.fileExtension)) {
    //   this.fileExtensionError = false;
    //   this.fileExtensionMessage = ""
    // } else {
    //   this.fileExtensionMessage = "Only Image allowed!!"
    //   return this.fileExtensionError = true;
    // }

    // if (this.Files.size > 10 * 1024 * 1024) {
    //   this.fileSizeError = true
    // }
    // if (this.Files.size <= 10 * 1024 * 1024) {
    //   this.fileSizeError = false
    // }
    // if (event.target.files[0] == '') {
    //   this.logoErrorMsg = true
    //   this.fileExtensionMessage = ""
    // }
    // if (event.target.files[0] != '') {
    //   this.logoErrorMsg = false
    // }



  }

  File() {

    const formData = new FormData();
    formData.append('Name', this.fileObj.addFileName);
    formData.append('orignalName', this.Files, this.Files.name);

    this.http.addFile(formData).subscribe(
      (resp) => {

        document.getElementById("addFileupload").click();
        // this.getAllTask();
      },
      (err) => {
        console.log(err);

      })
  }

  getAllFile() {

    this.http.getAllFile().subscribe(data => {

      console.log('DATA in file------->', data);


      let fileArr = []


      data.file.map(element => {

        fileArr.push(
          {

            'fileName': element.Name,
            'file': element.photo.orignalName,
            'blobname': element.photo.blobName,
            'createdAt': element.createdAt,
            'fileId':element._id
          })
      })
      this.rows = fileArr
      this.temp = fileArr
      console.log("data in task rows===>🎌🎌 ", this.rows);


    }, err => {
      console.log(err);
    })
  }

  getFileId(rowData) {
    this.fileId = rowData.fileId
    console.log(this.fileId);

  }
  deleteFile() {
    this.http.deleteFile(this.fileId).subscribe(
      (resp) => {
        document.getElementById("deleteFile").click();
        this.getAllFile();
      },
      (error) => {
        console.log(error);
      })
   }

  getUrl(rowData) {
    console.log("rowData===",rowData);

    this.blobnameFile = rowData.blobname
    console.log("hhhhhhhhhhh-----------------------------------------", this.blobnameFile);
    this.http.getLogoSASUrl(this.blobnameFile).subscribe(
      (resp) => {
        this.SASUrl = resp.url;
      },
      (error) => {
        console.log(error);
      })
  }
}
