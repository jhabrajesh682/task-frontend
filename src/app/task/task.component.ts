import { Component, OnInit } from '@angular/core';
import { SearchPipe } from '../common/pipe/search.pipe';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {

  closeResult = '';
  taskId: string
  filterSearch: string;
  taskObj: any = {}
  page = 1
  pageSize = 10
  data: any = []
  rows: any[] = []
  temp: any[] = []

  constructor(private http: HttpService,
    private searchPipe: SearchPipe) { }

  ngOnInit() {
    this.getAllTask();
  }


  search() {
    if (this.filterSearch.length == 0) {
      this.rows = this.temp;
      return;
    } else {
      this.rows = this.searchPipe.transform(this.temp, this.filterSearch);
    }
  }

  getAllTask() {

    this.http.getAllTask().subscribe(data => {

      console.log('DATA in roles------->', data);


      let taskArr = []
      console.log(data);

      data.task.map(element => {

        taskArr.push(
          {

            'taskName': element.TaskName,
            'projectName': element.projectName,
            'status': element.status,
            'description': element.Description,
            'taskId': element._id,
            'taskCreated': element.createdAt,
            'taskCompleted': element.updatedAt

          })
      })
      this.rows = taskArr
      this.temp = taskArr
      console.log("data in task rows===>🎌🎌 ", this.rows);


    }, err => {
      console.log(err);
    })
  }

  editModalData(rowData) {
      console.log("rowData====>", rowData);
      this.taskId = rowData.taskId
      this.taskObj.updateTask = rowData.taskName,
      this.taskObj.updateProject = rowData.projectName,
      this.taskObj.updateDescription = rowData.description,
      this.taskObj.updatestatus = rowData.status
  }

  Task() {

    let finalObj = {
      TaskName: this.taskObj.addTask,
      Description: this.taskObj.addProject,
      projectName: this.taskObj.addDescription,
      status: this.taskObj.status,
    }
    console.log("finalObj====>🛫🛫 ", finalObj);
    this.http.addTask(finalObj).subscribe(
      (resp) => {

        document.getElementById("addTask").click();
        this.getAllTask();
      },
      (err) => {
        console.log(err);

      })
  }
  addModalData() {
  this.taskObj.addTask = '',
  this.taskObj.addProject = '',
  this.taskObj.addDescription = '',
  this.taskObj.status = ''
  }

  TaskMarkUpdate() {
    let updateObj = {
      TaskName: this.taskObj.updateTask,
      projectName: this.taskObj.updateProject,
      Description: this.taskObj.updateDescription,
      status: this.taskObj.updatestatus
    }
    console.log("update Task=====>", updateObj);
    this.http.updateTask(updateObj, this.taskId).subscribe(
      (resp) => {

        document.getElementById("updateTask").click();
        this.getAllTask();
      },
      (err) => {
        console.log(err);

      })
  }

  getTaskId(rowData) {
    this.taskId = rowData.taskId
  }
  deleteTask() {
    this.http.deleteTask(this.taskId).subscribe(
      (resp) => {
        document.getElementById("deleteTask").click();
        this.getAllTask()
      },
      (error) => {
        console.log(error);
      })
  }
}
